import 'package:firebase_app/singup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './login.dart';
import './singup.dart';
import './home.dart';
import './add_contact.dart';
import './update_contact.dart';
import './main.dart';
import './CrudFire.dart';

 class Dashboard extends StatefulWidget { 
   _DashboardState createState() => _DashboardState();
 }
 
 class _DashboardState extends State<Dashboard> {
  CrudFire crud = new CrudFire();
  QuerySnapshot contacts;
  FirebaseUser user;
  final auth = FirebaseAuth.instance;
  Future<void> getUserData() async {
    FirebaseUser userData = await FirebaseAuth.instance.currentUser();
    setState(() {
      user = userData;
    });
  }

  @override
  void initState() { 
    super.initState();
    getUserData();
    crud.getData().then((data){
      setState(() {
        contacts = data;
      });
    });
  }

  Future<void> _singout() async{
    auth.signOut();
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Home()));
  }

  Widget showData(){
    if(contacts != null && contacts.documents != null){
        return ListView.builder(
            itemCount: contacts.documents.length,
            itemBuilder: (BuildContext context, index){
              return Container(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: (){
                          contacts.documents[index].reference.delete();
                        },
                      ),
                      leading: IconButton(
                        icon: Icon(Icons.edit),
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute
                            (builder: (context) => UpdateContact(contacts.documents[index])
                          ));
                        },
                      ),
                      title: Text('${contacts.documents[index].data['name']}'),
                      subtitle: Text('${contacts.documents[index].data['mobile']}'),
                    ),
                  ],
                ),
              );
            },
        );
    }else if(contacts != null && contacts.documents.length == 0){
      return Text('No data right now');
    }else{
      return Text('Pleas');
    }
  }

   @override
   Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
        title: Text('My Contacts App'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context){
                return AddContact();
              }));
            },
          ),
          FlatButton(
            child: Text('SingOut'),
            onPressed: _singout,
          ),
        ],
      ),
      body: showData(),
     );
   }
 }
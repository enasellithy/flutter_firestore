import 'package:flutter/material.dart';
import './main.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './home.dart';
import './dashboard.dart';

class Singup extends StatefulWidget {
  Singup({Key key}) : super(key: key);

  _SingupState createState() => _SingupState();
}

class _SingupState extends State<Singup> {
  final GlobalKey<FormState> _formstate = GlobalKey<FormState>();
  String _email, _password;

  Future<void> singup() async{
    final formdate = _formstate.currentState;
    if(formdate.validate()){
      formdate.save();
          FirebaseUser fireuser = await FirebaseAuth.instance.createUserWithEmailAndPassword(
            email: _email, password: _password,
          );

          fireuser.sendEmailVerification();

          Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Dashboard()),
            (Route<dynamic> route) => false);
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Singup'),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Form(
                  key: _formstate,
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            icon: Icon(Icons.email),
                            hintText: 'Email Address',
                          ),
                          validator: (val){
                            if(val.isEmpty){
                              return 'Please Enter Your Email';
                            }
                          },
                          onSaved: (val) => _email = val,
                        ),
                        TextFormField(
                          decoration: InputDecoration(
                            icon: Icon(Icons.vpn_key),
                            hintText: 'Password',
                          ),
                          obscureText: true,
                          validator: (val){
                            if(val.isEmpty){
                              return 'Please Enter Password';
                            }else if(val.length < 3){
                              return 'Your Password need to be atlesat 4 char';
                            }
                          },
                          onSaved: (val) => _password = val,
                        ),
                        RaisedButton(
                          color: Colors.lightBlueAccent,
                          textColor: Colors.white,
                          onPressed: singup,
                          child: Text('Singup'),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
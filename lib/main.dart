import 'package:flutter/material.dart';
import './login.dart';
import './home.dart';

void main() => runApp(MaterialApp(
  title: 'My Contacts App',
  debugShowCheckedModeBanner: false,
  theme: ThemeData.light(),
  home: Home(),
));

import 'package:firebase_app/dashboard.dart';
import 'package:firebase_app/singup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './login.dart';
import './singup.dart';
import './main.dart';

class Home extends StatefulWidget {

  const Home({
    Key key,
    this.user
  }) : super(key:key);

  final FirebaseUser user;

  //@override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  FirebaseUser user;
  final auth = FirebaseAuth.instance;
  Future<void> getUserData() async {
    FirebaseUser userData = await FirebaseAuth.instance.currentUser();
    setState(() {
      user = userData;
    });
  }

  @override
  void initState() { 
    super.initState();
    getUserData();
  }

  Future<void> _singout() async{
    auth.signOut();
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Login()));
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Contacts App'),
      ),
      body:Container(
        child: Column(
          children: <Widget>[
            user == null ?
             Column(
               children: <Widget>[
                  FlatButton(
                    child: Text('Login TO See Your Contacts'),
                    color: Colors.lightBlueAccent,
                    textColor: Colors.white,
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => Login()
                      ));
                    },
                  ),
                  FlatButton(
                    child: Text('Singup'),
                    color: Colors.lightBlueAccent,
                    textColor: Colors.white,
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                        builder: (context) => Singup()
                      ));
                    },
                  ),
               ],
             ):
            Column(
              children: <Widget>[
                Text('Your Email is ${user.email}'),
                FlatButton(
                    child: Text('Singout'),
                    color: Colors.lightBlueAccent,
                    textColor: Colors.white,
                    onPressed: _singout,
                ),
                FlatButton(
                    child: Text('Dashboard'),
                    color: Colors.lightBlueAccent,
                    textColor: Colors.white,
                    onPressed: (){
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Dashboard()));
                    },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}